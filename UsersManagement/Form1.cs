﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UsersManagement
{
    public partial class Form1 : Form
    {
        DBLinq dbHandler;
        string admins_table_name;

        public Form1()
        {
            InitializeComponent();

            admins_table_name = "admins";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dbHandler = new DBLinq();
            dbHandler.initializeConnection("localhost", "user_management_system", "root", "");
            dbHandler.connectToDB();

            string username = username_tbx.Text;
            string password = password_txb.Text;

            DBLinqModel model = new DBLinqModel();
            model.fields = new object[] {"username","password","age"};
            model.types = new string[] {DBLinqModel.Type.VARCHAR+"(50)", DBLinqModel.Type.VARCHAR+"(50)", DBLinqModel.Type.INT};
            dbHandler.createTable("test1", model);

//            dbHandler.getDataSet(admins_table_name, );

        }
    }
}
